package com.landlordassistant.application.embedded;

public enum Gender {
    MALE,
    FEMALE,
    OTHER,
    UNKNOWN
}
